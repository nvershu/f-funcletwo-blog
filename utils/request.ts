// request 是默认实例可直接使用, extend为可配置方法, 传入一系列默认参数, 返回一个新的request实例, 用法与request一致.
import  { extend } from 'umi-request';
const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  405: '请求方法不被允许。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};
const extendedRequest = extend({
  maxCache: 10, // 最大缓存个数, 超出后会自动清掉按时间最开始的一个.
  prefix: 'http://127.0.0.1:8001/', // prefix
  // suffix: '.json', // suffix
  errorHandler: (error) => {
    console.log(error)
    alert('接口错误')
  },
  timeout:3000
});
// request拦截器, 改变url 或 options.
extendedRequest.interceptors.request.use((url, options) => {
  // const token = localStorage.getItem('x-auth-token')   // 拿到 token
	// const headers = {'x-auth-token': token}
  let token = window.localStorage.getItem("accessToken")
  let headers = {}
  if(token){
    headers ={token:token}
  }
  else if(!url.includes('login')){
    throw new Error("没token");
  }
  console.log('url',url)
  return (
    {
      url,
      options: { ...options, headers}
    }
  );
});
 
// response拦截器, 处理response
extendedRequest.interceptors.response.use(async (response)=>{
  console.log('response',response)
  // if(response.code === '404'){
  const res = await response.clone().json();
  console.log('response1',res)
  const {code,message} = res
  if(code === 400){
    alert(message)
  }
  // }
  return response
}
  // response.headers.append('interceptors', 'yes yo');
  // response.headers.append('CORS_ALLOW_HEADERS' ,'default_headers')
  );
export default extendedRequest
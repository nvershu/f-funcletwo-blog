import _ from 'lodash'
import React from 'react';
import HeaderLayer from './components/headerLayer';
import styles from '../styles/index.module.scss'
interface state{
  show:Boolean
}
class Home extends React.Component <Object,state>{
  position:string = 'absolute'
  constructor(props:Object){
    super(props)
    this.state={
      show:false
    }
  }
  componentDidUpdate(){
    
  }
  componentDidMount(){
      window.addEventListener('scroll',_.debounce(this.auto,200))
      this.setState({
        show: Boolean(window.localStorage.getItem('show'))
      })
  }
  auto=()=>{
    window.pageYOffset>60 && (this.position = 'fixed')
  }
  render(){
    
    return (
    <>
    <HeaderLayer show={this.state.show}/>
    <div className={styles.homeContainer}>
        <div className={styles.containerLeft} >
          
        </div>
        <div className={styles.containerMiddle}>
          
        </div>
        <div className={styles.containerRight}>
          
        </div>
    </div>
    </>
  )
}
}

export default Home

import _ from 'lodash'
import Router from 'next/router';
import React from 'react';
import styles from '../../styles/login.module.scss'
import {fetchList,login,register} from '../api/login'
interface state{
  type:Boolean,
  username:string,
  password:string,
  email:string
}
class Login extends React.Component<any,state>{
    constructor(props:any){
        super(props)
        this.state={
            type:true,
            username:'',
            password:'',
            email:''
        }
    }
  componentDidMount(){
    if(typeof window !== undefined){
      
    }
  }
  setUsername=(e:any)=>{
    this.setState({
      username: e.target.value
    })
  }
  setPassword=(e:any)=>{
    this.setState({
      password: e.target.value
    })
  }
  setEmail=(e:any)=>{
    this.setState({
      email: e.target.value
    })
  }
  changeType=()=>{
    this.setState({
        type:!this.state.type
    })
  }
  login = ()=>{
    let data={
      username:this.state.username,
      password:this.state.password,
      email:this.state.email
    }
    console.log('data',data)
    if(this.state.type)
    login('login/',data).then((res)=>{
      if(res.data.token){
      window.localStorage.setItem('accessToken',JSON.stringify(res.data.token))
      window.localStorage.setItem('show','true')
      setTimeout(() => {
        Router.replace('/')
      }, 1000);
    }
    })
    else
    register('login/',data).then((res)=>{
      if(res.data.token){
      window.localStorage.setItem('accessToken',JSON.stringify(res.data.token))
      window.localStorage.setItem('show','true')
      setTimeout(() => {
        Router.replace('/')
      }, 1000);
    }
    })
  }
  render(){
    let title = '登录'
    let input 
    if(this.state.type){
    input = (
        <div>
            <h3>密码登录</h3>
            <input type="text" placeholder='用户名/邮箱' className={styles.rightInput} value={this.state.username} onChange={(e) => { this.setUsername(e) }}/>
            <input type="password" placeholder='密码' className={styles.rightInput} value={this.state.password} onChange={(e) => { this.setPassword(e) }}/> 
            <button className={styles.rightButton} onClick={this.login}>登录</button>
            <span>没有账号？<button  style={{'color':'#0bb2cf','border':'none'}} onClick={this.changeType}>立即注册</button></span>
         </div>
    )}
    else {
        input = (
            <div>
                <h3>立即注册</h3>
                <input type="text" placeholder='用户名' className={styles.rightInput} value={this.state.username} onChange={(e) => { this.setUsername(e) }}/>
                <input type="password" placeholder='密码' className={styles.rightInput} value={this.state.password} onChange={(e) => { this.setPassword(e) }}/> 
                <input type="text" placeholder='邮箱' className={styles.rightInput} value={this.state.email} onChange={(e) => { this.setEmail(e) }}/>
                <button className={styles.rightButton} onClick={this.login}>注册</button>
                <span>注册完就可以畅游循趣世界啦<button  style={{'color':'#0bb2cf','border':'none'}} onClick={this.changeType}>{'<<<返回'}</button></span>
             </div>
        )
    }
    return (
    <>
    <div className={styles.login}>
        <div className={styles.loginLeft} >
         
        </div>
        <div className={styles.loginRight} >
         <h1>{title}</h1>
         <i className={styles.rightLine}></i>
         {input}
        </div>
    </div>
    </>
  )
}
}

export default Login

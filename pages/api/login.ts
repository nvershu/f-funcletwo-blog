// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// import type { NextApiRequest, NextApiResponse } from 'next'
import request from '../../utils/request'
export function fetchList(url:string,data:Object){
return request(url,{
  data:data,
  method:'post'
})
}
export function login(url:string,params:Object){
  return request(url,{
    params:params,
    method:'get'
  })
}

export function register(url:string,data:Object){
    return request(url,{
      data:data,
      method:'post',
      requestType: 'form'
    })
}